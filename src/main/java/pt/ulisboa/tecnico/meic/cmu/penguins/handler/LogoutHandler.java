/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cmu.penguins.handler;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import pt.ulisboa.tecnico.meic.cmu.penguins.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.server.Server;

public class LogoutHandler implements HttpHandler {

  /*
   * Request type: POST
   * Headers:
   *    Authz: sessionId
   * Parameters: -
   *
   * Expected (JSON):
   *   -
   *
   * Returns (JSON):
   *   -
   *
   * Error:
   *   Returns error String
   * */

  @Override
  public void handle(HttpExchange httpExchange) throws IOException {
    if (httpExchange.getRequestMethod().equals("POST")) {
      System.out.println(
          String.format("LogoutHandler: Received POST from %s", httpExchange.getRemoteAddress()));
      String sessionId = httpExchange.getRequestHeaders().get("Authz").get(0);
      if (sessionId == null) {
        httpExchange.sendResponseHeaders(400, -1);
        System.out
            .println(String.format("Request from %s was invalid", httpExchange.getRemoteAddress()));
        return;
      }

      //TODO: hash password

      SessionFactory sessionFactory = Server.getSessionFactory();
      Session s = sessionFactory.openSession();
      Transaction t = null;
      String response = "Unknown error";
      int responseCode = 400;
      try {
        t = s.beginTransaction();
        String queryString = "FROM User U WHERE U.session.sessionId= :session";
        Query<User> query = s.createQuery(queryString, User.class)
            .setParameter("session", sessionId);
        if (query.list().size() != 1) {
          response = "Invalid code";
          responseCode = 403;
          return;
        }
        User user = query.list().get(0);
        user.setSession(new pt.ulisboa.tecnico.meic.cmu.penguins.model.Session());
        s.save(user);

        t.commit();

        response = "";
        responseCode = 200;

        System.out.println(String.format("LogoutHandler: %s has logged out with the username %s",
            httpExchange.getRemoteAddress(), user.getUsername()));
      } catch (HibernateException he) {
        responseCode = 400;
        if (t != null) {
          System.out.println(String.format("LogoutHandler: %s has failed the logout request",
              httpExchange.getRemoteAddress()));
          t.rollback();
        } else {
          System.out.println("LogoutHandler: transaction creation failed");
        }
      } finally {
        httpExchange.sendResponseHeaders(responseCode, response.getBytes().length);
        httpExchange.getResponseBody().write(response.getBytes());
        s.close();
        httpExchange.close();
      }
    } else {
      httpExchange.sendResponseHeaders(405, -1);
    }
  }
}
